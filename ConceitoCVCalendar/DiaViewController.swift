//
//  DiaViewController.swift
//  ConceitoCVCalendar
//
//  Created by Breno Pinheiro Aquino on 28/08/17.
//  Copyright © 2017 Breno Pinheiro Aquino. All rights reserved.
//

import UIKit
import CVCalendar

class DiaViewController: UIViewController {

    var date: DayView!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dateLabel.text = "\(String(date.date.day)) / \(String(date.date.month)) / \(String(date.date.year))"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
